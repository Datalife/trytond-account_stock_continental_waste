# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import location
from . import stock

def register():
    Pool.register(
        location.Location,
        location.LocationAccount,
        stock.Move,
        module='account_stock_continental_waste', type_='model')
