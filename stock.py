# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.pool import Pool, PoolMeta


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    def _get_account_stock_move_type(self):
        move_type = super()._get_account_stock_move_type()
        if move_type and move_type.endswith('_lost_found'):
            if move_type.startswith('in_'):
                location = self.from_location
            else:
                location = self.to_location
            if location.waste:
                move_type = move_type.replace('_lost_found', '_waste')
        return move_type

    def _get_account_stock_move_lines(self, type_):
        pool = Pool()
        Uom = pool.get('product.uom')
        AccountMoveLine = pool.get('account.move.line')

        if type_ and (type_.endswith('_lost_found')
                or type_.endswith('_waste')):

            assert type_.startswith('in_') or type_.startswith('out_'), \
                'wrong type'
            # code repeated from account_stock_continental
            # to avoid AccountError when no accounts in/out on product
            move_line = AccountMoveLine()
            unit_price = Uom.compute_price(self.product.default_uom,
                    self.cost_price, self.uom)
            amount = self.company.currency.round(
                    Decimal(str(self.quantity)) * unit_price)

            if type_.startswith('in_'):
                move_line.debit = Decimal('0.0')
                move_line.credit = amount
                move_line.account = self.from_location.account_stock_in_used
            else:
                move_line.debit = amount
                move_line.credit = Decimal('0.0')
                move_line.account = self.to_location.account_stock_out_used

            return [move_line]
        else:
            return super()._get_account_stock_move_lines(type_)
