datalife_account_stock_continental_waste
========================================

The account_stock_continental_waste module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-account_stock_continental_waste/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-account_stock_continental_waste)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
